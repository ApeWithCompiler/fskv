package fskv

import (
    "testing"
)

func Equal(a, b []byte) bool {
    if len(a) != len(b) {
        return false
    }
    for i, v := range a {
        if v != b[i] {
            return false
        }
    }
    return true
}

type KvEngineTestAgent struct {
    Key string
    Data []byte
    T *testing.T
}

func (a KvEngineTestAgent) Write(key string, data []byte) error {
    if key != a.Key {
        a.T.Errorf("Write() key was %v, want %v", key, a.Key)
    }

    if !Equal(data, a.Data) {
        a.T.Errorf("Write() data was %v, want %v", data, a.Data)
    }

    return nil
}

func (a KvEngineTestAgent) Read(key string) ([]byte, error) {
    if key != a.Key {
        a.T.Errorf("Read() key was %v, want %v", key, a.Key)
    }

    return []byte("foobar"), nil
}

func (a KvEngineTestAgent) Delete(key string) error {
    if key != a.Key {
        a.T.Errorf("Delete() key was %v, want %v", key, a.Key)
    }

    return nil
}


func TestInitialization_DefaultPath(t *testing.T) {
    sut := FsKv{}

    expect := "data/default"
    
    if sut.getPath() != expect {
        t.Errorf("getPath() returned %v, want %v", sut.getPath(), expect)
    }
}

func TestInitialization_CustomPath(t *testing.T) {
    sut := FsKv{
        Options: FsKvOptions {
            Basepath: "/srv/data",
            Database: "test1",
        },
    }

    expect := "/srv/data/test1"

    if sut.getPath() != expect {
        t.Errorf("getPath() returned %v, want %v", sut.getPath(), expect)
    }
}

func TestSet(t *testing.T) {
    agent := KvEngineTestAgent {
        Key: "/srv/data/test1/foo/bar",
        Data: []byte("foobar"),
    }

    sut := FsKv{
        Options: FsKvOptions {
            Basepath: "/srv/data",
            Database: "test1",
            Engine: agent,
        },
    }

    sut.Set("foo/bar", []byte("foobar"))
}

func TestUnset(t *testing.T) {
    agent := KvEngineTestAgent {
        Key: "/srv/data/test1/foo/bar",
    }

    sut := FsKv{
        Options: FsKvOptions {
            Basepath: "/srv/data",
            Database: "test1",
            Engine: agent,
        },
    }

    sut.Unset("foo/bar")
}

func TestGet(t *testing.T) {
    agent := KvEngineTestAgent {
        Key: "/srv/data/test1/foo/bar",
    }

    sut := FsKv{
        Options: FsKvOptions {
            Basepath: "/srv/data",
            Database: "test1",
            Engine: agent,
        },
    }

    sut.Get("foo/bar")
}