package fskv

import (
	"log"
	"path"
)

const (
	DEFAULT_BASEPATH string = "data"
	DEFAULT_DATABASE string = "default"
)

type AbstractKvEngine interface {
	Write(key string, data []byte) error
	Read (key string) ([]byte, error)
	Delete(key string) error
}

type FsKvOptions struct {
	Basepath string
	Database string
	Engine AbstractKvEngine
}

func (opt *FsKvOptions) getBasepath() string {
	if opt.Basepath != "" {
		return opt.Basepath
	}

	return DEFAULT_BASEPATH
}

func (opt *FsKvOptions) getDatabase() string {
	if opt.Database != "" {
		return opt.Database
	}

	return DEFAULT_DATABASE
}

type FsKv struct {
	Options FsKvOptions
}

func (f *FsKv) Set(key string, data []byte) error {
	k := path.Join(f.getPath(), key)

	err := f.Options.Engine.Write(k, data)
	if err != nil {
		log.Printf("WARNING FsKv engine Write() error for key %s", key)
	}

	return err
}

func (f *FsKv) Unset(key string) error {
	k := path.Join(f.getPath(), key)

	err := f.Options.Engine.Delete(k)
	if err != nil {
		log.Printf("WARNING FsKv engine Delete() error for key %s", key)
	}

	return err
}

func (f *FsKv) Get(key string) ([]byte,error) {
	k := path.Join(f.getPath(), key)

	data, err := f.Options.Engine.Read(k)

	if err != nil {
		log.Printf("WARNING FsKv engine Read() error for key %s", key)
	}

	return data, err
}

func (f *FsKv) IsSet(key string) bool {
	k := path.Join(f.getPath(), key)

	return f.Options.Engine.Exists(k)
}

func (f *FsKv) getPath() string {
	return path.Join(f.Options.getBasepath(), f.Options.getDatabase())
}
