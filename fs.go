package fskv

import (
	"os"
	"path/filepath"
)

type KvSyncFsEngine struct {

}

func (e KvSyncFsEngine) Write(key string, data []byte) error {
	dir, _ := filepath.Split(key)
	os.MkdirAll(dir, os.ModePerm)

    return os.WriteFile(key, data, 0644)
}

func (e KvSyncFsEngine) Read(key string) ([]byte, error) {
    return os.ReadFile(key)
}

func (e KvSyncFsEngine) Delete(key string) error {
    return os.Remove(key)
}

func (e KvSyncFsEngine) Exists(key string) bool  {
    if _, err := os.Stat(key); err == nil {
	return true
    }

    return false
}
