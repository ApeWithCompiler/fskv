package fskv

import (
    "path"
)

func KeySplit(key string) string {
    return path.Join(key[0:2], key[2:4], key[4:])
}